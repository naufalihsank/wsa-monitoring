// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ['~/styles/tailwind.css'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  modules: ['nuxt-swiper', '@nuxtjs/google-fonts'],
  googleFonts: {
    families: {
      Poppins: true,
      'Days+One': true,
    }
  }
})
